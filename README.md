# Roblox Studio Installation on the Chromebook Through Grape Juice in 5 Steps

Lemme walk you through it...

# Step 1: Starting Up Linux:
Open Linux Terminal (If your on a chromebook, and don't have it, go to settings, click on the menu icon, click on Linux (Beta) and install,
you will then have the terminal)

# Step 2: Downloading WINE:
Now before we start I would like you to know,
WINE is an Acronym for Wine Is Not an Emulator,
the name it's self (I'm guessing) is supposed to be short for WINdows with a little Extra (WIN-E),
it's also the system that will be running Grape Juice (Which I'm sure is supposed to the "Kids" Version of WINE,
because wine in real life is alchoholic Grape Juice.),
which is going to be running Roblox Studio.

Now, type, "sudo dpkg --add-architecture i386" (This part opens the gate to do most of the stuff!), followed by, "sudo apt update"
(This part is very important, as it saves only the stuff that works right!),
again, followed by, "sudo apt install wine". Now wait,
when you can type again,type "sudo apt-add-repository 'deb https://dl.winehq.org/wine-builds/ubuntu/ disco main'"

# Step 3: Python Download for Chromebooks:
AGAIN, in the terminal,
type "sudo apt install build-essential libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev zlib1g",
once again you need to wait after every single one of these so I'm going to stop saying it for now,
type "wget https://www.python.org/ftp/python/3.7.3/Python-3.7.3.tgz", followed by, "tar xvf python-3.7.3.tgz",
and that's followed by, "cd python-3.7.3', then followed by, "./configure", and then, "make",
and finally "sudo make install"! Now you Chromebook Users Have Python V. 3.7.3!

# Step 4: (Installation of Grape Juice):
 First type in the terminal,
"sudo apt install python3-pip virtualenv libcairo2-dev libgirepository1.0-dev libgtk-3-0 libgtk-3-bin libdbus-1-dev",
next type, "cd . .", followed by "wget https://gitlab.com/brinkervii/grapejuice/-/archive/master/grapejuice-master.zip",
then type, "unzip grapejuice-master.zip", and then, "cd grapejuice-master", and finally,
"python3.7 ./install.py"!

# Step 5 (Debugging of Grape Juice for Chromebook):
Ok so you need to locate Grape Juice,
by either searching for it or finding it in Linux Apps, if you try to open it,
you can't, soooo, we need to fix this, and to do so,
type "cat /etc/apt/sources.list.d/winehq.list" (This will allow access to fix your WINE version!),
next type, "sudo apt update" to save that information,
then you need to type, "sudo apt purge wine" to go back to the original version while still saving all of the original information,
and finally type, "sudo apt install --install-recommends winehq-stable",
once done you should be able to open Grape Juice! Click on it and go to the Maintenance Section while there click Install Roblox,
this will prompt you to download 2 things, you may get 3, but anyways install them all!

These things are what make Roblox Studio Work on the Chromebook, After you're done click Install Roblox again,
this time it will install the Roblox Player (Which does nothing), next go back to the Launcher Section, and Click Roblox Studio,
this time it will finally download! You may notice typing your information and entering will bring you to the website,
only to be greeted by, ChromeOS can not open this page..
Which we can bypass but we haven't figured that one out yet..